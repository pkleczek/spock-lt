#Lighting talk - Spock Framework

Spock is a modern test framework written in Groovy.
Thanks to that you can create flexible and easy to read tests and assertions.

Paweł, don't forget about:

1. Pretty reports
2. Assertion evaluator
3. jUnit @Parametrized sucks

To learn more visit:
* https://github.com/spockframework/spock-example
* http://www.petrikainulainen.net/programming/testing/writing-unit-tests-with-spock-framework-introduction-to-specifications-part-one/


