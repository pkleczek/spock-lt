package pgs.spock;

//From https://github.com/junit-team/junit4/wiki/Parameterized-tests

public class Fibonacci {
    public static int compute(int n) {
        int result = 0;

        if (n <= 1) {
            result = n;
        } else {
            result = compute(n - 1) + compute(n - 2);
        }

        return result;
    }
}
