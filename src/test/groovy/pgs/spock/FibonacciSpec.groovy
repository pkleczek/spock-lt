package pgs.spock

import spock.lang.Specification
import spock.lang.Unroll

class FibonacciSpec extends Specification {
    @Unroll
    def "fib(#n) == #expected"(n, expected) {
        expect:
        Fibonacci.compute(n) == expected

        where:
        n << [0, 1, 2, 3, 4, 5, 6]
        expected << [0, 1, 1, 2, 3, 5, 8]
    }
}
