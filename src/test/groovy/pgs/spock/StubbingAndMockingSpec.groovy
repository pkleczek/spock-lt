package pgs.spock

import spock.lang.Specification
import spock.lang.Unroll

class CreditInfoService {
    boolean validate(int amount, int age) {
        return Math.random() > 0.5
    }
}

class CreditRatingCalc2 {
    def externalInfo = new CreditInfoService()

    boolean canHaveCredit(int amount, int age) {
        return externalInfo.validate(amount, age)
    }
}


class MockingSpec extends Specification {
    CreditRatingCalc2 ratingCalc

    def setup() {
        ratingCalc = new CreditRatingCalc2()
        ratingCalc.externalInfo = Mock(CreditInfoService)
    }

    def "when validation is performed a call to service is done"() {
        when:
        ratingCalc.canHaveCredit(25, 1000)

        then:
        1 * ratingCalc.externalInfo.validate(25, 1000)

    }

}

class StubbingSpec extends Specification {
    CreditRatingCalc2 ratingCalc

    def setup() {
        ratingCalc = new CreditRatingCalc2()
        ratingCalc.externalInfo = Stub(CreditInfoService)
    }

    @Unroll
    def "test outcome depending on external service"(external, expected) {
        when:
        ratingCalc.externalInfo.validate(_, _) >> external

        then:
        ratingCalc.canHaveCredit(25, 1000) == expected

        where:
        external || expected
        false    || false
        true     || true
    }

}

