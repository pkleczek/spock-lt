package pgs.spock

import spock.lang.Specification

class HelloSpec extends Specification {

    def "check if addition works fine"() {
        expect:
        2 + 2 == 4
    }

    def "check if multiplication works fine"() {
        given:
        def a = 7, b = 6

        when:
        def result = 7 * 6

        then:
        result == 42
    }

    def "more detailed explanations"() {
        def text = "ala"

        expect:
        (text * 3).toUpperCase() + text[0..1] == "ALAALAALAal"
    }

}
