package pgs.spock

import spock.lang.Specification
import spock.lang.Unroll

class CreditRatingCalc {
    CreditRatingCalc() {
        println "Initializing!"
    }

    boolean canHaveCredit(int amount, int age) {
        if (amount < 0)
            throw new IllegalArgumentException("Amount can't be lower than zero.")
        return age < 75 && amount < 10000
    }

}

class SeriousBusinessSpec extends Specification {
    def calc = new CreditRatingCalc()

    def "an exception is thrown when amount is lower than zero"() {
        when: "you invoke the method with amount lower than zero"
        calc.canHaveCredit(-1, 29)

        then:
        def e = thrown(IllegalArgumentException)
        e.message == "Amount can't be lower than zero."
    }

    @Unroll
    def "check if you can get credit for #amount when you are #age yo"(amount, age, expected) {
        expect:
        calc.canHaveCredit(amount, age) == expected

        where:
        amount  | age || expected
        1000    | 25  || true
        2000    | 76  || false
        100_000 | 17  || false

    }

}
